

# Modern robotics

## Vector

A vector is **not** a set of numbers, for it to be that it needs a base.

## Vector space
A vector space has a scalar product and a vector sum.

It does **not** have an inner product which means that things like orthogonality are
not defined in general.

Vector spaces have an origin and a dimension.

Vectors scaled by scalars or added stay in the vector space

## Free vectors

Vectors may or may not have an origin, vectors without an origin are called
free vectors which means that they can be moved around by parallel transport 
in **non-curved* spaces**.

For example, a velocity means the same thing independent of its origin.

## Base

A base is a bijective relation between $\mathcal{V}^n$ and $R^n$. It is defined by
a set of linearly independent vectors. Each vector space can have multiple bases
and a transformation between them is denoted by a $R^{n\times n}$ matrix


A base is a non-singular matrix meaning that it is invertible $\Leftrightarrow det(A) \neq 0$ 
A singular vector has at least two linearly dependent columns or rows.

A vector with n linearly independent columns has n linearly independent rows as well.

## Co-vectors

A vector can be written as rows of numbers or columns of numbers. The representations
makes no difference mathematically, but does in physics.

A co-vecctor can be seen as a linear operator from a vector space to a real number.

Row vectors are co-vectors and column vectors are vectors. Multiplication of
a row vector and a column vector gives a real number

One can also view a vector as a linear operator from a covector to a number.

## Coordinate system transformations

Coordinate systems should not affect things like power. i.e. power in one system should
be the same as power in another system.

This means that

$$
F^Tv = \bar{F}^T\bar{v}
$$

let $v = A\bar{v}$ then
$$
F^TA\bar{v} = \bar{F}^T\bar{v}
$$

This means that
$$
F^TA = \bar{F}^T \Leftrightarrow F = A^{-T}\bar{F}
$$

But this is not the same as in the original equation $v=\bar{A}v$. It is only
the same if A is orthonormal

## Tensor
Vectors, matrixes and more-dimensional structures are varying orders of tensors. A vector
is order 1, a matrix is order 2 and so on.

Tensors have a type
$$
\left(
\begin{matrix}
p \\
q \\
\end{matrix}
\right)
$$

A tensor is a **multilinear operator**

A vector is a (1,0) tensor while a co-vector is (0,1).

A matrix is a vector where $p+q = 2$ and it can be seen as a linear operator from vectors and covectors to vectors or numbers.

Eigenvalues only make sense for some matrixes, specifically those which are tensors
from vector to vector or co-vector to co-vector. **Is this correct**?

Einstein notation can be used as statically typed tensors.

## Manifolds

A manifold is a non-linear surface.

You can map a local region of the surface to $\mathbf{R}^n$.



## Tangent space

Since manifolds are non-linear, normal tools do not work. However, in every point on 
the surface, we can define a tangent-space which is a linear vector-space that is 
tangental to the surface.

As an example we can talk about linear velocity on the ground even though the earth is 
a sphere.

Movement can be described as a trajectory or a sequence of points no the manifold.

Different points have different tangent spaces but points close to each other have 
similar spaces.

Vectors in a tangent space are bound to the corresponding point

There is also a co-tangent space which contains co-vectors.

Given a manifold $\mathcal{M}$:

Each position $m \in \mathcal{M}$ is a configuration of the manifold.

The tangent space associated with $m$ on the manifold $M$ is denoted by $T_m \mathcal{M}$

Hence, the a velocity $v$ at point p would be $v \in T_m\mathcal{M}$ **question** Is T_m 
and M a product or is this simply notation?

Similarly, a vector $\tau$ in the co-tangent space is given by $\tau \in T_m^*M$

Co-vectors and vectors can only be operated on if they are part of the tangent and 
co-tangent space at the same point

## Groups

A group is a set of objects along with a binary operator from 2 elements to a third.

This operator has 
 - to be associative i.e. $(AB)C = A(BC)$
 - have an identity element such that $AI = A$
 - have an inverse and be bijective, i.e. $AA^{-1} = I$


## Lie group

A lie group is a group that is also a manifold. This means that it should have an
identity point. The tangent (and co-tangent (**is this true?**)) space is the lie algebra.

An angular velocity is an example of a quantity in the identity point. Therefore integration
of angular velocity makes no sense.

## Euclidian spaces

**question** Are euclidean spaces always $R^3$?

An euclidean space is a manifold. However, it is not curved **?**

The euclidean space contains a set of points $\mathcal{E}$ and a set of free vectors
$\mathcal{E}^*$.

An inner product is an operator $\mathcal{E} \to \mathcal{R}$

The inner product is denoted by $\langle,\rangle$

We can define the length of a vector as

$$
||\cdot||: \mathcal{E}^* -> R ; v \mapsto \sqrt{\langle v, v \rangle}
$$

Cosine and orthogonality can also be defined completely in terms of inner vector product.

## Coordinate systems

Physical points are denoted by $p, p_i, q_i$ etc.

The points expressed in $\mathcal{J}$ are denoted by $p_i^\mathcal{J}$


Coordinate systems (in 3d) are a n+1-tuple. On origin and 3 basis vectors.

$\Psi = (0, e_1, e_2, e_3,...)$ or commonly x,y,z hat 

A coordinate system is orthonormal if $||e_i|| = 1$, i.e. the length of all basis vectors
is 1, and $\langle e_j, e_i \rangle = 0$  i.e. the basises are orthogonal.

As a consequence, orthonormality requires a euclidean space.

A change of basis can be described as

$R^n \to \mathcal{E} \to R^n ; p^1 \mapsto \mathcal{E} \mapsto p^2$

Where the mapping is $\psi^-1$ for the first transformation

Vector product is also defined in terms of IP

$$\langle (a \wedge b), a \rangle = 0$$
$$\langle (a \wedge b), b \rangle = 0$$
$$|| a \wedge b || =$$ **question** what? ^^

# Rotation

## in 2d

Given a frame $\Psi_1$, a point $p$ has a position. The coordinates of that position can be
expressed by

$$
p^1 = \left(
    \begin{matrix}
        x_1 \\
        y_1
    \end{matrix}
\right)
=
\left(
    \begin{matrix}
        \langle (p-o_1), \hat{x_1} \rangle \\
        \langle (p-o_1), \hat{y_1} \rangle
    \end{matrix}
\right)
$$


Equivalently, in a different frame $\psi_2$:

$$
p^1 = \left(
    \begin{matrix}
        x_2 \\
        y_2
    \end{matrix}
\right)
=
\left(
    \begin{matrix}
        \langle (p-o_2), \hat{x_1} \rangle \\
        \langle (p-o_2), \hat{y_1} \rangle
    \end{matrix}
\right)
$$

This is the same thing as saying

$$
(p-o_1) = x_1 \hat{x_1} + y_1 \hat{y_1}
$$
and
$$
(p-o_2) = x_2 \hat{x_2} + y_2 \hat{y_2}
$$

Since these are the same points, they should also be identical

Combining these (how?)

We get

$$
\left(
    \begin{matrix}
        x_2 \\
        y_2
    \end{matrix}
\right)
=
\left(
    \begin{matrix}
        \langle \hat{x_1}, \hat{x_2} \rangle & \langle \hat{y_1}, \hat{x_2} \rangle \\
        \langle \hat{x_1}, \hat{y_2} \rangle & \langle \hat{y_1}, \hat{y_2} \rangle
    \end{matrix}
\right)
\left(
    \begin{matrix}
        x_1 \\
        x_2
    \end{matrix}
\right)
$$

The matrix above transforms coordinates from frame 1 into frame 2 is denoted by $R^2_1$ 

Worked out, it is also

$$
\left(
    \begin{matrix}
        \cos(\alpha)  & \sin(\alpha) \\
        -\sin(\alpha) & \cos(\alpha)
    \end{matrix}
\right)
$$

$\det(R_1^2) = 1$ because of the trigonometric identity

The inverse is equal to the transpose because it is a rotation matrix

Both the columns and rows have length 1

## 3d
With this insight and without further calculation, the 3d case can be infered as

$$
\left(
    \begin{matrix}
        \langle \hat{x_1}, \hat{x_2} \rangle 
            & \langle \hat{y_1}, \hat{x_2} \rangle
            & \langle \hat{z_1}, \hat{x_2} \rangle
            \\
        \langle \hat{x_1}, \hat{y_2} \rangle
            & \langle \hat{y_1}, \hat{y_2} \rangle
            & \langle \hat{z_1}, \hat{y_2} \rangle
            \\
        \langle \hat{x_1}, \hat{z_2} \rangle
            & \langle \hat{y_1}, \hat{z_2} \rangle
            & \langle \hat{z_1}, \hat{z_2} \rangle
            \\
    \end{matrix}
\right)
$$

Rotation along a single axis does not change the coordinates of that axis.

Therefore, rotation along the y-axis for example is

$$
\left(
    \begin{matrix}
        . & 0 & . \\
        0 & 1 & 0 \\
        . & 0 & .
    \end{matrix}
\right)
$$

Where the dots are the elements from the 2d rotation matrix

## The special orthonormal group - SO(3)
A set of all possible rotation matricies can be constructed as

$$
SO(3) := \{ R : R^{-1} = R^T , \det(R) = 1\}
$$

Both of these are required, for example, $\det(R) = -1$ is a reflection

The group SO(3) is called the "special orthonormal group" and is a lie group

$SO(3)$ is a manifold and $so(3)$ is an algebra **Is it a lie algebra?**.


If rotation is smooth, all the elements in $R$ are smooth. Then

$\dot{R}R^T$ and $R^T \dot{R}$ are skew symetric and belong to $so(3)$

**Show skew symetry**

For each $\omega_1, \omega_2 \in \mathbb{R}^3$ we have

$\tilde{\omega}_1 = \dot{R}R^T$ and $\tilde{\omega}_2 = R^T \dot{R}$


$so(3)$ is a lie algebra because a linear combination of skew-symetric matricies
is still skew-symetric.

To each $\Omega \in so(3)$  we can associate an $\omega \in \mathbb{R}^3$ such that
$\tilde{\omega}=\Omega$

We can then define the commutation operator $[]$ as follows:

$$
\Omega_1, \Omega_2 \in SO(3)
$$

$$
[,] \to SO(3) : [\Omega_1, \Omega_2] = \Omega_1 \Omega_2 - \Omega_2 \Omega_1
$$


The chain rule (of rotation) says that

$$
R^n_1 = R^n_{n-1} .. R^2_1
$$



### Relationship between $R$ and $\omega$

$SO(3)$ is a manifold. Each point $R(t)$ contains a tangent space which also has a 'velocity' $\dot{R(t)}$

$so(3)$ is the lie-algebra of $SO(3)$ and $\omega_R(t)$ is the angular velocity at $R(T)$ relative to
$R$ **What does this mean?**


## Motivation for $so(3)$

The lie group structure above makes it possible to reason about rotation without a specific
current rotation $R$ using $\omega$

On the other hand, if you want to integrate the rotation, you need $R$ and $\dot{R}$ instead

## Relation between $\omega$, $R$ and $\dot{R}$

$$
\tilde{\omega} = R^{-1} \dot{R}
$$

Which allows us to go between the three.

## Quaternions

Rotation matricies only have 3 independent dimensions rather than the 9 variables possible

Quaternions are 4d-vectors consisting of a scalar indicating the amount of rotation around
an axis. It is a hypersphere in 4-d


It can also be expressed as a 2-by-2 matrix of imaginary numbers

$$
\begin{pmatrix}
    a+ib & c+id \\
    -c+id & a-ib
\end{pmatrix}
$$

where

$$
a^2 + b^2 + c^2 + d^2 = 1
$$

The set of quaternions is called $SU(2)$ and $SU(2)$ is a double cover of $SO(3)$


Quaternions are simply connected while rotation matrices are not. Meaning that any loop
can be fully closed on any point on the manifold.


# General motion

We want a general formula which transforms the coordinates of a point $p$ from $\psi_1$ to $\psi_2$

It will look like

$p^1 = A p^2 + B$

This formula must obviously hold for all points

Finding b is trivial, select $p = o_2$ where $o_2$ is the origin of the second frame

$$
p = O_2 \to p^1 = o_2^2 =
\begin{pmatrix}
    0 \\
    0
\end{pmatrix}
$$

Then, because $p^2 = 0$

$$
p^1 = A p^2 + B \implies o_2^1 = B
$$

To find A, we use the basis vectors of the other frame to find each column.

The transformation matrix between two frames expresses the coordinates of the basis
vectors in the first frame in the second.

## Orthonormal change of coordinates

Rotation and translation can be described as

$$
p^j = R^j_i p^i + o^j_i
$$

$$
R^i_j \in SO(3), o^j_i \in \mathbb{R}^3
$$


However, this structure makes chaining and inversion harder


## Homogenous Matrixes

We want to just use matrix multiplication to encode both rotation and translation.

To do so, we transform vectors and matricies to homogeneous form

$$
p =
\begin{pmatrix}
    x \\
    y \\
    z
\end{pmatrix}
=
\begin{pmatrix}
    x \\
    y \\
    z \\
    1
\end{pmatrix}
$$

$$
H^j_i =
\begin{pmatrix}
    R^j_i & o^j_i \\
    0     & 1
\end{pmatrix}
$$

Geometrically, this is the coordinates expressed in a 4-d space, then projected
back into a 3d subspace at distance 1 from the "projector"


- *Sidenote*: No rotation is represented by $I$ rather than $0$
- *Sidenote 2*: When projections are present, the bottom row does not have to be $(0, 0, 0, 1)$

Homogenous matricies are not orthonormal which means that $R^{-1} =! R^T$

## Inverting

$$
( H^1_2 )^{-1} = H^2_1 \implies H^1_2 H^2_1 = I_4
$$


$$
H^1_2 =
\begin{pmatrix}
    (R^2_1)^T & -(R^2_1)^T o^2_1 \\
    0 & 1
\end{pmatrix}
$$

## The special euclidean group $SE(3)$

$$
SE(3) :=
\left\{
     \begin{pmatrix}
         R & t \\
         0 & 1
     \end{pmatrix}
     s.t.
     R \in SO(3),
     o \in \mathbb{R}^3
\right\}
$$

It is a group because of associativity, identity and invertability.

## se(3) and twists

$H(t) \in SE(3)$ is differentiable and $\dot{H} H^{-1}$ as well as $u^{-1} \dot{H}$ belong to $se(3)$

$$
se(3) =
\left\{
    \begin{pmatrix}
        \tilde{\omega} & v \\
        0 & 0
    \end{pmatrix}
    \text{such that }
    \tilde{\omega} \in so(3)
    \text{ and }
    v \in \mathbb{R}^3
\right\}
$$

**Note the 0 in the bottom right**. This is an important difference between SE(3) and se(3) which
comes from the fact that $1' = 0$


Since $\omega$ and $v$ are each vectors of 3 elements, we can define $T$ as

$$
T :=
\begin{pmatrix}
    \omega \\
    v
\end{pmatrix}
\tilde{T} \in
\begin{pmatrix}
    \tilde{\omega} & v \\
    0 & 0
\end{pmatrix}
\text{ for }
$$

Which is another way of expressing elements in $se(3)$. Elements in $se(3)$ are called twists





## Indices

Twists are the velocities and wrenches are the forces of 6d rotation and translation.

Velocities must have a reference frame and an object that they affect. Velocity also isn't
absolute so twists need 3 indices.

## Rotation and angular velocity

Rotation between 2 frames is described by a rotation matrix $\in$ SO(3)

If two frames are rotating relative to each other, the rotation can be described
as a function of time $R(t)$. We can then take the derivative of this and get
$\dot{R}(t)$

$R(t)$ is a path along a manifold so $\dot{R}(t)$ is also part of SO(3) and
the rotation at $R(t)$ on that manifold



Since SO(3) is a lie group, we can use the inverse of a rotation matrix to transport
it back to the lie algebra. Hence, we can express angular velocity in the identity
as follows:

$$
R^{-1}\dot{R} = \omega
$$

and

$$
\dot{R} R^{-1} = \omega
$$

*Note* while they are transformed into the same space, pre- and post-multiplications
give different omegas. They get expressed in different reference frames


This allows us to compare angular velocities in a way that makes sense

## Twists

A twist is a rotation and velocity which is independent of H ($H \in SE(3)$).

It is similar to angular velocities in that you use the inverse of H to transport
$\dot{H}$ to the lie algebra of SE(3) where they can be compared.


The velocity component of a twist is the velocity of a theoretical point in the
origin of the reference frame on a rigid body that is affected by the twist.

The twist on all parts of a rigid body is the same


## Wrenches

Wrenches represent a torque and a force acting on an object. Like force and velocity,

Wrenches and forces multiplied give a power which is a real number.

Wrenches are row matrices since they are co-vectors (multiplying them with
a vector gives a real number)

Just like normal co-vectors, coordinate transformation of wrenches is not the same
as coordinate transformations of the corresponding twist. The power scalar produced
must be the same in all coordinate systems

